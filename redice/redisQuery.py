import redis
import json
# import datetime
from datetime.datetime import now
from mysql.connector import connect, Error
from phpserialize import unserialize, phpobject
from parsers import gost_parser
import traceback
import logrus


class RedisQyery:
    def __init__(self) -> None:
        self.r = redis.Redis()
        self.namespace_default = "laravel_database_queues:default"
        self.namespace_notify = "laravel_database_queues:default:notify"
        self.namespace_reserved = "laravel_database_queues:default:reserved"

    def extract_data_from_redis(self):
        notify = self.r.lpop(self.namespace_notify)
        if notify != None:  # нотифи не пустой
            default = self.r.lpop(self.namespace_default)
            default_dict = json.loads(default.decode('utf-8'))

            self.r.zadd(self.namespace_reserved, {
                json.dumps(default_dict).encode('utf-8'): now().timestamp()
            })

            return default_dict
        return None

    def extract_text_from_file(self, value):
        """ Принимает `value` из метода `transform_from_phpserialize`
            Загрузит файл только в том случае, если запустить локально
        """
        with open(value['data']['command']['path']) as f:
            return f.readlines()

    def transform_from_phpserialize(self, value):
        data_command = value['data']['command']
        data_command_byte = bytes(data_command, 'utf-8')

        output = unserialize(data_command_byte, object_hook=phpobject)

        output = output._asdict()
        output = {
            key.decode(): val.decode() if isinstance(val, bytes) else val
            for key, val in output.items()
        }
        value['data']['command'] = output

        return value

    def get_keys_from_text(self, text, value):
        request_list = []

        for rec in gost_parser.findall(text):
            request_facts = {
                "name": rec.fact.Name,
                "code": rec.fact.Code,
                "year": rec.fact.Year,
                "path_file": value['data']['command']['path'],
                "position": {

                }
            }

            for pos, name in zip(rec.fact.spans, request_facts):
                request_facts["position"][name] = pos

            request_list.append(request_facts)

        return request_list

    def load_error_data(self, value, error: str):
        try:
            with connect(
                host="localhost",
                user="root",
                password="",
                database="labrary_nir",
            ) as connection:

                insert_reviewers_query = f"""
                INSERT INTO failed_jobs (uuid, connection, queue, payload, exception)
                VALUES (%s,"redice", "python", %s, %s)
                """
                val = (value['uuid'], json.dumps(value), error)

                with connection.cursor() as cursor:
                    cursor.execute(insert_reviewers_query, val)
                    connection.commit()

        except Error as e:
            logrus.Log(e, traceback.format_exc())

    def load_keys(self, list_keys):
        print(f"ТИПА отправка данных егору: {list_keys}")

    def run(self):
        value_phpseroalize = self.extract_data_redisseroalize()
        if value_phpseroalize != None:
            try:
                value_dict = self.transform_from_phpserialize(
                    value_phpseroalize)
                text_file = self.extract_text_from_file(value_dict)
                keys_from_file = self.get_keys_from_text(text_file, value_dict)
                self.load_keys(keys_from_file)

                return True
            except Exception as e:
                self.load_error_data(value_phpseroalize, str(e))
            finally:
                self.r.zrem(self.namespace_reserved, json.dumps(
                    value_phpseroalize).encode('utf-8'))

        return False
