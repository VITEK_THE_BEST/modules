import imp
from yargy import Parser, rule, and_, not_, token
from yargy.interpretation import fact
from yargy.predicates import gram, dictionary, type
from yargy.relations import gnc_relation
from yargy.pipelines import morph_pipeline

PREFIX = morph_pipeline(['согласно', 'во исполнении'])

BASE_NAME = morph_pipeline(['гост', 'национальный стандарт российской федерации', 'межгосударственный стандарт'])

CODE_PARSER = rule(type('INT'), rule('.').optional()).repeatable(0, 3)

GOST_FACT = fact('gost', ['Name', 'Code', 'Year'])

DELIMITERS = dictionary(['-', '/', ' '])

gnc = gnc_relation()
GOST = rule(
    PREFIX.optional(),
    BASE_NAME.interpretation(GOST_FACT.Name),
    rule('Р').optional(),
    CODE_PARSER.interpretation(GOST_FACT.Code),
    rule(DELIMITERS).optional(),
    type('INT').interpretation(GOST_FACT.Year).optional(),
).interpretation(GOST_FACT)

gost_parser = Parser(GOST)




