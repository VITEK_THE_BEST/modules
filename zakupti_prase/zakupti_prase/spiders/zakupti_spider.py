from urllib import response
import scrapy
import re
from urllib.parse import urlparse, parse_qs, urlencode


class QuotesSpider(scrapy.Spider):
    name = "zakupki"
    custom_settings = {
        # 'CLOSESPIDER_PAGECOUNT': 2,
        # "DEPTH_LIMIT": 10_000,
        'CONCURRENT_REQUESTS': 2,
        'URLLENGTH_LIMIT': 10_000,
    }

    def start_requests(self):
        urls = [
            'https://zakupki.gov.ru/epz/order/extendedsearch/results.html?morphology=on&sortBy=UPDATE_DATE&pageNumber=1&sortDirection=false&recordsPerPage=_10&showLotsInfoHidden=false&fz44=on&fz223=on&ppRf615=on&fz94=on&af=on&ca=on&pc=on&pa=on&priceContractAdvantages44IdNameHidden=%7B%7D&priceContractAdvantages94IdNameHidden=%7B%7D&priceFromGWS=Минимальнаяцена&priceFromUnitGWS=Минимальнаяцена&priceToGWS=Максимальнаяцена&priceToUnitGWS=Максимальнаяцена&currencyIdGeneral=-1&customerPlace=5277381&selectedSubjectsIdNameHidden=%7B%7D&okdpGroupIdsIdNameHidden=%7B%7D&koksIdsIdNameHidden=%7B%7D&OrderPlacementSmallBusinessSubject=on&OrderPlacementRnpData=on&OrderPlacementExecutionRequirement=on&orderPlacement94_0=0&orderPlacement94_1=0&orderPlacement94_2=0&contractPriceCurrencyId=-1&budgetLevelIdNameHidden=%7B%7D&nonBudgetTypesIdNameHidden=%7B%7D',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def page_processing(self, response):
        current_page = int(response.css(
            '.page__link_active > span::text').get() or '-1')

        url = urlparse(response.url)
        params = parse_qs(url.query)
        page_number = int(params.get('pageNumber', ['-1'])[0])

        if current_page != page_number:
            raise Exception('End pages')

        # params['pageNumber'] = int(page_number) + 1

        next_page_url = response.url.replace(
            f'pageNumber={current_page}', f'pageNumber={page_number + 1}')

        yield scrapy.Request(url=next_page_url, callback=self.page_processing)

        urls = response.css(
            '.search-registry-entry-block > div > div > div.registry-entry__header > div > div.registry-entry__header-mid__number > a').xpath('@href').getall()
        for url in urls:
            yield response.follow(url=url, callback=self.redirect_to_documents)

    def parse(self, response):
        yield from self.page_processing(response)

    def redirect_to_documents(self, response):
        redirect_url = response.css(
            'div.cardHeaderBlock > div > div > a').xpath('//*[contains(normalize-space(text()), "Документ")]').xpath('@href').get()
        if not redirect_url:
            raise Exception('Not found doc url')
        yield response.follow(url=redirect_url, callback=self.prase_order_docs)

    def prase_order_docs(self, response):
        docs = response.css(
            '.attachment__value > div > span > a').xpath('@href').getall()
        if not docs:
            docs = response.css('.blockFilesTabDocs > div > div > span > a').xpath(
                '@href').getall()
        # if not docs:
        #     docs = response.css('').xpath('@href').getall()
        if not docs:
            yield {'url': '', 'isFind': False, 'parent_url': response.url}
        for doc in docs:
            if doc.find('modal=true') != -1:
                continue
            yield {'url': doc, 'isFind': True, 'parent_url': response.url}


if __name__ == '__main__':
    from scrapy.crawler import CrawlerProcess

    process = CrawlerProcess()
    process.crawl(QuotesSpider)
    process.start()
